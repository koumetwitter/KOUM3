# KOUM3 [![Build status](https://ci.appveyor.com/api/projects/status/we3q9gnpdsu975ed?svg=true)](https://ci.appveyor.com/project/Ren/koum3)
***A cool thing that posts pictures or something.***

### Options

- **Twitter**
  - Twitter keys and shit. [こちら。](https://dev.twitter.com/oauth/overview/application-owner-access-tokens)
- **App**
  - *image_folder*: where images go
  - *execution_chance*: self explanatory
  - *allow_repeat_after*: can't repeat until x # of posts
  - *log_file*: path to logs, like *BOT_PATH/logs/log*. 
  - *bot_account*: bot @
  - *master_account*: your @
  - *dont_tweet_file*: no-no images, *BOT_PATH/logs/banned*
- **Commands**
  - *ban_command*: delete previous image
  - *request_command*: gives user an image
  - *time_tolerance*: how old for request until it is ignored
- **Text**
  - *tweet_post_number*: True/False
  - *tweet_this_text*: if you want a personalized message
  - *requests_answers*: list of random responses
  - *requests_to_third_answers*: responses for requests to a third person


### Example
```
# Replace the appropriate fields with your settings.

[Twitter]
api_key = your_api_key
secret_key = your_secret_key
token = your_token
secret_token = your_secret_token

[App]
image_folder = /home/botImages/
execution_chance = 1
allow_repeat_after = 50
log_file = /home/bot/logs/log
bot_account = @KOUM3
master_account = @your_twitter_account
dont_tweet_file = /home/bot/logs/banned

[Commands]
#caps agnostic
ban_command = @KOUM3 rm
request_command = @KOUM3 rq
time_tolerance = 10

[Texts]
tweet_post_number = True
tweet_this_text = 
request_answers = Just for you!

request_to_third_answers = This is from 
```

Backwards Compatibilty
===============

Please note that if you were using a previous version of the bot, you'll have 
to execute the bot once like this from the terminal after enabling the option. 
This is important if you want to use the tweet_post_number option.  

```python twitterbot.py --tweet --tweetnumber X```


Where $`X = current img+1`$

This will just log the tweet so that the bot can read the post number from the 
log, so it will know now how many posts there have been so far. 
The `--tweet` option is there just to force the bot to tweet
and ignore the execution chance.

Usage
===============

Execute it w/ python3, preferably via a cron job.
Depending on your settings, it won't post each time it's activated.

A few arguments that can be added:

```
optional arguments:
  -h, --help  Shows help message
  --tweet     Posts image
  --test      Write to log (causes img# to go up)
  --postnumber Enables post number 
```